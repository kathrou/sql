SELECT * FROM ventas;






-- Identificar registros con valores nulos
SELECT nombre, email
FROM ventas
WHERE email IS NULL;







-- Cambiar blancos a nulos
WIth datos_limpios AS (
  
  SELECT 
  	nombre,
   	CASE WHEN email = ''
  		THEN NULL 
  	END AS email_limpio
  FROM ventas
)

SELECT nombre, email_limpio
FROM datos_limpios
WHERE email_limpio IS NULL; 



  
  


-- Seleccionar filas con correos electrónicos mal formados
SELECT *
FROM ventas
WHERE email NOT LIKE '%_@__%.__%';







-- Seleccionar filas con valores faltantes en columnas clave
SELECT *
FROM ventas
WHERE cantidad IS NULL 
   OR region IS NULL 
   OR producto IS NULL 
   OR email IS NULL;
   
   
   
   
   
   
   -- Corregir cantidades negativas o marcarlas como devoluciones
SELECT 
	id_venta, 
    nombre, 
    email, 
    fecha_venta, 
    cantidad,
    CASE
     WHEN cantidad < 0 THEN ABS(cantidad) -- Si quieres hacer los valores positivos
       ELSE cantidad
    END AS cantidad_corregida,
    region, 
    producto, 
    comentarios
FROM ventas;







-- Eliminar espacios en blanco en las columnas
SELECT id_venta, 
       TRIM(nombre) AS nombre_limpio, 
       TRIM(email) AS email_limpio,
       fecha_venta, 
       cantidad, 
       TRIM(region) AS region_limpia, 
       TRIM(producto) AS producto_limpio, 
       TRIM(comentarios) AS comentarios_limpios
FROM ventas;





-- Identificar la venta más reciente de cada cliente
WITH RankedVentas AS (
    SELECT id_venta, nombre, email, producto, region, fecha_venta, cantidad,
           ROW_NUMBER() OVER (PARTITION BY email ORDER BY fecha_venta DESC) AS rn
    FROM ventas
)
SELECT *
FROM RankedVentas
WHERE rn = 1;




-- Ventas con comentarios importantes (Alta prioridad, Queja del cliente)
SELECT *
FROM ventas
WHERE comentarios LIKE '%Alta prioridad%' OR comentarios LIKE '%Queja del cliente%';






-- Agrupar ventas por rangos de cantidad
SELECT 
    CASE 
        WHEN cantidad BETWEEN 0 AND 100 THEN '0-100'
        WHEN cantidad BETWEEN 101 AND 200 THEN '101-200'
        WHEN cantidad BETWEEN 201 AND 300 THEN '201-300'
        ELSE 'Más de 300'
    END AS rango_cantidad,
    COUNT(*) AS numero_ventas
FROM ventas
WHERE cantidad > 0
GROUP BY rango_cantidad;


--CREAR NUEVA TABLA LIMPIA

CREATE TABLE ventas_limpias AS
  SELECT 
  	id_venta,
    nombre, 
    CASE WHEN email = ''
  		THEN NULL 
  	END AS email_limpio,
    fecha_venta, 
    cantidad,
    CASE
     WHEN cantidad < 0 THEN 'devolucion'-- Si quieres hacer los valores positivos
       ELSE 'compra'
    END AS estado_de_venta,
    region,
    producto,
    comentarios
  FROM ventas
;

--- ANALISIS

--Análisis de cantidad total de ventas por región

SELECT region, SUM(cantidad) AS total_ventas
FROM ventas
GROUP BY region;


--Porcentaje de participación de cada región en las ventas totales
SELECT region, 
       SUM(cantidad) AS total_ventas,
       (SUM(cantidad) / (SELECT SUM(cantidad) FROM ventas)) * 100 AS porcentaje_participacion
FROM ventas
GROUP BY region;

--Clientes con más ventas
SELECT nombre, email, COUNT(*) AS numero_ventas
FROM ventas
GROUP BY nombre, email
HAVING COUNT(*) > 1;

--Devoluciones por producto
SELECT producto, COUNT(*) AS devoluciones
FROM ventas
WHERE cantidad < 0
GROUP BY producto;


-- Suma acumulada de las ventas de bolsos a lo largo del tiempo
SELECT producto, fecha_venta, cantidad,
       SUM(cantidad) OVER (PARTITION BY producto ORDER BY fecha_venta ASC) AS suma_acumulada
FROM ventas
WHERE producto = 'Bolso'
ORDER BY fecha_venta ASC;